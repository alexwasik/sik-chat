# sik-chat


> WasikChat

# A Real-Time Chat App with Vue + Firebase

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## DB Setup
This repo utilizes Firebase.

Create a Firebase db and set your project database to "Cloud Firestore" in the Firebase Console.

Create the config file `src/config.js`.

Example:
```
export default {
  apiKey: "your apiKey",
  authDomain: "your authDomain",
  databaseURL: "your databaseURL",
  projectId: "your projectId",
  storageBucket: "your storageBucket",
  messagingSenderId: "your messagingSenderId"
};
```

## Init
The Initialize DB button will create seed data in the Firebase DB. 

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
