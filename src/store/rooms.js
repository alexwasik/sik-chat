import Vue from 'vue'
import uuidv4 from 'uuid/v4'

const state = {
  all: {},
  allRooms: [],
  roomMessages: [],
  selectedRoom: ''
}

const mutations = {
  SET_ROOM (state, { room }) {
    const data = room.data()
    data['id'] = room.id
    state.all = {
      ...state.all,
      [room.id]: { name: data.name, created: data.created, messages: data.messages }
    }
    state.allRooms.push(data)
    state.selectedRoom = state.allRooms[0].id
    localStorage.setItem('selectedRoom', state.selectedRoom)
  },
  SET_ACTIVE_ROOM (state, id) {
    state.selectedRoom = id.roomId
    localStorage.setItem('selectedRoom', state.selectedRoom)
  },
  GET_ROOM_MESSAGES(state, payload) {
    state.roomMessages = payload.messages
  },
  ADD_MESSAGE (state, { message }) {
    let msg = state.roomMessages.filter(vendor => (vendor.id === message.id));
    debugger;
    if (!msg.length) {
      state.all[state.selectedRoom].messages.push(message)
      state.roomMessages.push(message)
    }
  }

}

const actions = {
  sendMessage ({ commit, rootState }, { text, created, sender, roomId }) {
    console.log('Yay, we made it!', roomId);
    const convoRef = rootState.db.collection('rooms').doc(roomId)
    convoRef.update({
      messages: [...state.all[roomId].messages, { id: uuidv4(), created, sender, text }]
    })
    .then(res => console.log('Message sent.'))
    .catch(err => console.log('Error', err))
  },
  seed ({ rootState }) {
    let roomRef = rootState.db.collection('rooms')

    roomRef.add({
      name: 'Room 1',
      created: Date.now(),
      users: ['mr_a', 'mrs_a'],
      messages: [
        { id: uuidv4(), text: "It's Me!", sender: 'mr_a', created: Date.now() },
        { id: uuidv4(), text: "Hey You", sender: 'mrs_a', created: Date.now() }
      ]
    })

    roomRef.add({
      name: 'Room 2',
      created: Date.now(),
      users: ['mr_c', 'mrs_a'],
      messages: [
        { id: uuidv4(), text: "It's Kai!", sender: 'mr_c', created: Date.now() },
        { id: uuidv4(), text: "Clean Your Room", sender: 'mrs_a', created: Date.now() }
      ]
    })
  },

  async getRooms ({ commit, rootState }) {
    let roomRef = rootState.db.collection('rooms')
    let rooms = await roomRef.get()
    console.log('rooms',rooms);
    rooms.forEach(room => commit('SET_ROOM', { room } ))
  },
  async getRoomMessages({ commit, rootState }) {
    if (state.selectedRoom) {
      let roomRef = rootState.db.collection('rooms').doc(state.selectedRoom)
      let messageRef = await roomRef.get()
        .then(doc => {
          if(!doc.exists) {
            console.log('doc not found');
          } else {
            console.log('Doc Data:', doc.data());
            commit('GET_ROOM_MESSAGES', doc.data())
          }
        })
    }
  },
  setActiveRoom ({ commit }, { roomId }) {
    commit('SET_ACTIVE_ROOM', { roomId })
  }
}

export default { namespaced: true, state, mutations, actions}
