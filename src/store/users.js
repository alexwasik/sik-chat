const state = {
  all: {},
  currentUser: 'mr_a'
}

const mutations = {
  SET_USER (state, { user }) {
    state.all = {...state.all, [user.id]: user.data() }
  }
}

const actions = {
  seed ({ rootState }) {
    let userRef = rootState.db.collection('users')

    userRef.doc('mr_a').set({
      first_name: 'Alex',
      last_name: 'Wasik'
    })

    userRef.doc('mrs_a').set({
      first_name: 'Lindsay',
      last_name: 'Wasik'
    })

    userRef.doc('mr_c').set({
      first_name: 'Kai',
      last_name: 'Wasik'
    })
  },
  
  async get ({ commit, rootState }) {
    let userRef = rootState.db.collection('users')
    let users = await userRef.get()

    users.forEach(user => commit('SET_USER', { user }))
  }
}

export default {
  namespaced: true, state, mutations, actions
}
